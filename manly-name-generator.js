const config = require('./config');
const pick = require('lodash.sample');
const has = require('lodash.contains');
const intersection = require('lodash.intersection');
const startCase = require('lodash.startcase');
const sprintf = require('sprintf-js').sprintf;
const Discord = require('discord.js');
const fs = require('fs');
const child_process = require('child_process');
const usage = fs.readFileSync('./usage');
const emitter = require('events');

class suggestionBox extends emitter {}

const suggestion_box = new suggestionBox();
const editor_list = config.adminList.concat(config.editorList);

var names = {};
var suggestion_list = {};

suggestion_box.on('suggested-change', function(guild){
	fs.writeFile('./suggestions/%s.json'.sprintf(guild.id), JSON.stringify(suggestion_list[guild.id]), function(err){
		if (err){
			console.log(err);
			console.log('Failed to record new suggestions: %s'.sprintf(suggestion_list[guild.id].join(', ')));
		}
	});
});

suggestion_box.on('accepted-change', function(guild, accepted_list){
	names[guild.id] = names[guild.id].concat(accepted_list);
	fs.writeFile('./names/%s.json'.sprintf(guild.id), JSON.stringify(names[guild.id]), function(err){
		if (err){
			console.log(err);
			console.log('Failed to record new names: %s'.sprintf(accepted_list.join(', ')));
		}
	});
});

var bot = new Discord.Client();

bot.on('error', function(err){
	console.log(err.message, 'Unexpected error');
});

bot.once('ready', function(){
	bot.guilds.forEach(function(guild){
		names[guild.id] = read_json_list('names', guild.id);
		suggestion_list[guild.id] =  read_json_list('suggestions', guild.id);
	});

	var manly_names = bot.channels.find(function(c){
		return c.name === 'manly-names';
	});

	if (typeof manly_names !== 'undefined'){
		manly_names.send('Running version %s'.sprintf(config.version))
			.catch(err => console.log(err));
	}
});

bot.on('message', function(message){
	if (
		(
			   config.adminList.has(userID(message.author))
			|| config.adminList.intersects(roleList(message.member))
		)
	){
		switch (message.content){
			case '!stop': {
				message.delete()
					.catch(err => console.log(err));
				message.channel.send('Goodbye')
					.catch(err => console.log(err));
				bot.destroy()
					.catch(err => console.log(err));
				return;
			}
			case '!restart': {
				message.delete()
					.catch(err => console.log(err));
				message.channel.send('Goodbye')
					.catch(err => console.log(err));
				bot.destroy()
					.then(() => child_process.spawn('/home/manlynamesbot/launch', {
						'detached': true,
						'stdio': 'ignore'
					}).unref())
					.catch(err => console.log(err));
				return;
			}
			default: {
				break;
			}
		}
	}
	if (!(message.author.equals(bot.user)) && is_control_message(message)){
		var command_list = message.content.split(' ').map(s => s.trim());
		var command = command_list.shift();
		switch (command){
			case '!name': {
				var name_prefix_list = command_list.map(prefix => prefix.toLowerCase());
				delete_message(message);
				message.channel.send(generate_manly_name(message.guild, name_prefix_list))
					.catch(err => console.log(err));
				break;
			}
			case '!list': {
				if (command_list.length < 1){
					command_list.push('names');
				}
				var type = command_list.shift();
				switch (type){
					case 'names': {
						if (names[message.guild.id].length > 0){
							message.channel.send('```%s```'.sprintf(names[message.guild.id].sort().map(s => s.toTitleCase()).join(', ')))
								.catch(err => console.log(err));
						} else {
							message.channel.send('The name list is empty')
								.catch(err => console.log(err));
						}
						break;
					}
					case 'suggestions': {
						if (suggestion_list[message.guild.id].length > 0){
							message.channel.send('```%s```'.sprintf(suggestion_list[message.guild.id].sort().map(s => s.toTitleCase()).join(', ')))
								.catch(err => console.log(err));
						} else {
							message.channel.send('The suggestion list is empty')
								.catch(err => console.log(err));
						}
						break;
					}
					case 'count': {
						message.channel.send('%d name fragments are in the list'.sprintf(names[message.guild.id].length))
							.catch(err => console.log(err));
						break;
					}
					case 'classify': {
						var output = names[message.guild.id]
							.sort()
							.classify(s => s.charAt(0))
							.map(l => l.length)
							.kv()
							.rotor(2)
							.map(l => l.join(': '))
							.join("\n");

						message.channel.send('```%s```'.sprintf(output))
							.catch(err => console.log(err));
						break;
					}
					default: {
						message.channel.send('Unrecognized argument to !list: %s'.sprintf(type))
							.catch(err => console.log(err));
						break;
					}
				}
				break;
			}
			case '!suggest': {
				if (command_list.length < 1){
					message.channel.send('Not enough arguments to %s'.sprintf(command))
						.catch(err => console.log(err));
					break;
				}
				var added_list = [];
				while (s = command_list.shift()){
					s = s.toLowerCase();
					if (!names[message.guild.id].has(s) && !suggestion_list[message.guild.id].has(s)){
						suggestion_list[message.guild.id].push(s);
						added_list.push(s);
					}
				}
				suggestion_box.emit('suggested-change', message.guild);
				if (added_list.length > 0){
					message.channel.send('Added %s to the suggestion list'.sprintf(added_list.join(', ')))
						.catch(err => console.log(err));
				} else {
					message.channel.send('No suggestions added')
						.catch(err => console.log(err));
				}
				break;
			}
			case '!reject': {
				if (!editor_list.intersects(roleList(message.member))){
					message.channel.send('%s: permission denied'.sprintf(command))
						.catch(err => console.log(err));
					break;
				}
				if (command_list.length < 1){
					message.channel.send('Not enough arguments to %s'.sprintf(command))
						.catch(err => console.log(err));
					break;
				}
				var rejected_list = [];
				while (s = command_list.shift()){
					s = s.toLowerCase();
					if (suggestion_list[message.guild.id].has(s)){
						suggestion_list[message.guild.id].remove(s);
						rejected_list.push(s);
					}
				}
				suggestion_box.emit('suggested-change', message.guild);
				if (rejected_list.length > 0){
					message.channel.send('Rejected %s'.sprintf(rejected_list.join(', ')))
						.catch(err => console.log(err));
				} else {
					message.channel.send('No suggestions removed')
						.catch(err => console.log(err));
				}
				break;
			}
			case '!accept': {
				if (!editor_list.intersects(roleList(message.member))){
					message.channel.send('%s: permission denied'.sprintf(command))
						.catch(err => console.log(err));
					break;
				}
				if (command_list.length < 1){
					message.channel.send('Not enough arguments to %s'.sprintf(command))
						.catch(err => console.log(err));
					break;
				}
				var accepted_list = [];
				var current = names[message.guild.id].concat(accepted_list);
				var added_list = [];
				while (s = command_list.shift()){
					s = s.toLowerCase();
					if (suggestion_list[message.guild.id].has(s)){
						suggestion_list[message.guild.id].remove(s);
						if (!current.has(s)){
							accepted_list.push(s);
							added_list.push(s);
						}
					}
				}
				
				if (added_list.length > 0){
					message.channel.send('Accepted %s'.sprintf(added_list.join(', ')))
						.catch(err => console.log(err));
				} else {
					message.channel.send('No new names accepted')
						.catch(err => console.log(err));
				}
				suggestion_box.emit('suggested-change', message.guild);
				suggestion_box.emit('accepted-change', message.guild, accepted_list);
				break;
			}
			case '!version': {
				message.channel.send('```%s```'.sprintf(config.version))
					.catch(err => console.log(err));
				break;
			}
			case '!help': {
				message.channel.send('```%s```'.sprintf(usage))
					.catch(err => console.log(err));
				break;
			}
			case undefined:
			default: {
				// what?
				console.log('Unrecognized command: %s', message.content);
				message.channel.send('%s: unrecognized commmand'.sprintf(command))
					.catch(err => console.log(err));
				break;
			}
		}
	}
});

// method definitions. This could be a class or something, really.
var generate_manly_name = function(guild, name_prefix_list){
	var first;
	var sanity_check = 0;
	while (true){
		first = names[guild.id].pick();
		if (name_prefix_list.length === 0 || name_prefix_list.map(prefix => first.toLowerCase().startsWith(prefix)).reduce(function(car, cons){
			if (cons){
				car = cons;
			}
			return car;
		}, false)){
			break;
		}
		if (++sanity_check > 100){
			return 'no matching name found.';
		}
	}
	first = first.toTitleCase();
	if (Math.random() > 0.5){
		first += names[guild.id].pick().toLowerCase();
	}

	var last = '';
	if (Math.random() > 0.75){
		last += 'Mc';
	}

	last += names[guild.id].pick().toTitleCase() + names[guild.id].pick().toLowerCase();
	return '%s %s'.sprintf(first, last);
};

var is_control_message = function(message){
	return message.content.startsWith('!');
};

var userID = function(user){
	return '%s#%s'.sprintf(user.username, user.discriminator);
};

var roleList = function(user){
	return user.roles.map(role => role.name);
};

var delete_message = function(message){
	if (message.deletable){
		message.delete()
			.catch(err => console.log(err));
	}
};

var read_json_list = function(type, id){
	var fileTemplate = './%s/%s.json';
	var tmp = [];
	var jsonFile = fileTemplate.sprintf(type, id);
	if (fs.existsSync(jsonFile)){
		tmp = require(jsonFile).map(s => s.toLowerCase());
	}
	return tmp;
};

// extending the Array, String, and Object prototypes for fun, profit, and perl6 APIs.
// This should almost certainly be done in a module

Object.defineProperty(Array.prototype, 'first', {
	'value': function(){
		if (this.length > 0){
			return this[0];
		}
		return undefined;
	},
	'configurable': true,
	'enumerable': false
});

Object.defineProperty(Array.prototype, 'has', {
	'value': function(){
		var args = Array.prototype.slice.call(arguments);
		args.unshift(this);
		return has.apply(this, args);
	},
	'configurable': true,
	'enumerable': false
});

Object.defineProperty(Array.prototype, 'remove', {
	'value': function(){
		var item = arguments[0];
		if (typeof item === 'undefined'){
			throw 'Exactly one argument to Array.remove is required';
		}
		var i = this.indexOf(item);
		if (i > -1){
			this.splice(i, 1);
		}
	},
	'configurable': true,
	'enumerable': false
});

Object.defineProperty(Array.prototype, 'pick', {
	'value': function(){
		var args = Array.prototype.slice.call(arguments);
		args.unshift(this);
		return pick.apply(this, args);
	},
	'configurable': true,
	'enumerable': false
});

Object.defineProperty(Array.prototype, 'intersection', {
	'value': function(){
		var args = Array.prototype.slice.call(arguments);
		args.unshift(this);
		return intersection.apply(this, args);
	},
	'configurable': true,
	'enumerable': false
});

Object.defineProperty(Array.prototype, 'intersects', {
	'value': function(){
		var args = Array.prototype.slice.call(arguments);
		args.unshift(this);
		return intersection.apply(this, args).length > 0;
	},
	'configurable': true,
	'enumerable': false
});

Object.defineProperty(Object.prototype, 'kv', {
	'value': function(){
		var a = [];
		for (var key in this){
			a.push(key);
			a.push(this[key]);
		}
		return a;
	},
	'configurable': true,
	'enumerable': false
});

Object.defineProperty(Array.prototype, 'rotor', {
	'value': function(num){
		var out = [];
		if (this.length < 1){
			throw 'Cannot rotor an array of length 0';
		}
		for (var i = 0; i < this.length; i++){
			var a = [];
			for (var j = 0; j < num && i < this.length; j++){
				a.push(this[i++]);
			}
			i--;
			out.push(a);
			
		}
		return out;
	},
	'configurable': true,
	'enumerable': false
});

Object.defineProperty(Array.prototype, 'classify', {
	'value': function(test, opts){
		if (typeof test !== 'function'){
			throw 'Agument 1 to classify (test) must be a function';
		}
		opts = opts || {};
		var as = opts.as || function(v){ return v};
		if (typeof as !== 'function'){
			throw 'Named argument "as" must be a function';
		}
		var into = opts.into || null;
		if (into === null){
			into = {};
		}
		for (var i = 0; i < this.length; i++){
			value = this[i];
			var tested = test.call(this, value);
			value = as.call(this, this[i]);
			if (typeof into[tested] === 'undefined'){
				into[tested] = [];
			}
			into[tested].push(value);
		}
		return into;
	},
	'configurable': true,
	'enumerable': false
});

Object.defineProperty(Object.prototype, 'map', {
	'value': function(fn){
		if (typeof fn !== 'function'){
			throw 'The first argument to map must be a function: fn(value, key)';
		}

		var m = new Map(Object.entries(this));
		var o = {};
		m.forEach(function(value, key){
			o[key] = fn.call(this, value, key);
		});
		return o;
	},
	'configurable': true,
	'enumerable': false
});


Object.defineProperty(String.prototype, 'sprintf', {
	'value': function(){
		var args = Array.prototype.slice.call(arguments);
		args.unshift(this);
		return sprintf.apply(this, args);
	},
	'configurable': true,
	'enumerable': false
});

Object.defineProperty(String.prototype, 'toTitleCase', {
	'value': function(){
		var args = Array.prototype.slice.call(arguments);
		args.unshift(this);
		return startCase.apply(this, args);
	},
	'configurable': true,
	'enumerable': false
});

// main
bot.login(config.token)
	.catch(err => console.log(err));

